#!/usr/bin/python3
import random
def main():
    score = 0
    # LOL
    animal_dict = {
        "bear": "What likes to eat fish and hibernates during the winter?",
        "fish": "What swims in the sea and eats plankton and algae?",
        "fly": "What flys and lays eggs in stuff that needs to decompose?",
        "elephant": "What is the largest land animal with big ears and a trunk?",
        "snake": "What slithers on the ground and can be venomous or non-venomous?",
        "eagle": "What is a large bird of prey known for its keen eyesight and powerful beak?",
        "dolphin": "What is a highly intelligent marine mammal known for its playful behavior?",
        "lion": "What is often referred to as the king of the jungle and is a big cat species?",
        "tiger": "What is a large carnivorous cat with distinctive orange fur and black stripes?",
        "penguin": "What is a flightless bird that is well-adapted to life in the cold Antarctic?",
        "koala": "What is a marsupial native to Australia known for its tree-dwelling lifestyle?",
        "rhinoceros": "What is a large herbivorous mammal with one or two horns on its snout?",
        "giraffe": "What is the tallest living terrestrial animal with a long neck and spotted coat?",
        "crocodile": "What is a large aquatic reptile with a long tail, powerful jaws, and tough skin?",
        "kangaroo": "What is a marsupial known for its powerful hind legs and distinctive hopping?",
        "octopus": "What is a marine animal with a soft body, eight tentacles, and a beak?",
        "panda": "What is a bear native to China known for its black and white fur?",
        "camel": "What is a large, hump-backed mammal adapted to desert environments?",
        "hippopotamus": "What is a large, mostly herbivorous mammal with a barrel-shaped body?",
        "shark": "What is a predatory fish with cartilaginous skeletons and multiple rows of teeth?",
        "armadillo": "What is a small to medium-sized mammal with a protective bony armor?",
        "chimpanzee": "What is an intelligent primate known for using tools and social behavior?",
        "penguin": "What is a flightless bird that is well-adapted to life in the cold Antarctic?",
        "zebra": "What is a striped equine native to Africa with a distinctive black and white pattern?",
        "gorilla": "What is a large primate known for its strength and close genetic relation to humans?",
        "cheetah": "What is a fast-running big cat known for its distinctive black tear stripes?",
        "jaguar": "What is a large feline native to the Americas with a robust build and powerful jaws?",
        "otter": "What is a semiaquatic mammal with a sleek body and webbed feet?",
        "hedgehog": "What is a small nocturnal mammal covered in spines for protection?",
        "platypus": "What is a unique egg-laying mammal native to Australia with a duck-like bill?",
        "parrot": "What is a colorful, intelligent bird known for its ability to mimic human speech?",
        "gazelle": "What is a slender, fast-running antelope with distinctive curved horns?",
        "ostrich": "What is a large flightless bird with long legs and neck native to Africa?",
        "sloth": "What is a slow-moving mammal known for spending most of its life hanging from trees?",
        "mongoose": "What is a small carnivorous mammal known for its agility and ability to fight snakes?",
        "porcupine": "What is a rodent with sharp spines used for self-defense?",
        "kangaroo": "What is a marsupial known for its powerful hind legs and distinctive hopping?",
        "lemur": "What is a primate native to Madagascar known for its large eyes and bushy tail?",
        "whale": "What is a marine mammal that includes species like the blue whale and killer whale?",
        "squirrel": "What is a small, agile rodent with a bushy tail, known for hoarding nuts?",
        "wolf": "What is a carnivorous mammal of the canine family known for its pack behavior?",
        "moose": "What is a large deer species with palmate antlers native to North America and Eurasia?",
        "beaver": "What is a large, semiaquatic rodent known for building dams and lodges?",
        "penguin": "What is a flightless bird that is well-adapted to life in the cold Antarctic?",
        "kangaroo": "What is a marsupial known for its powerful hind legs and distinctive hopping?",
        "giraffe": "What is the tallest living terrestrial animal with a long neck and spotted coat?",
        "dolphin": "What is a highly intelligent marine mammal known for its playful behavior?",
        "snake": "What slithers on the ground and can be venomous or non-venomous?",
        "eagle": "What is a large bird of prey known for its keen eyesight and powerful beak?",
        "elephant": "What is the largest land animal with big ears and a trunk?",
        "panda": "What is a bear native to China known for its black and white fur?",
        "rhinoceros": "What is a large herbivorous mammal with one or two horns on its snout?",
        "lion": "What is often referred to as the king of the jungle and is a big cat species?",
        "tiger": "What is a large carnivorous cat with distinctive orange fur and black stripes?",
        "ostrich": "What is a large flightless bird with long legs and neck native to Africa?"
    }
    random_animal_dict_keys = list(animal_dict.keys())
    random.shuffle(random_animal_dict_keys)
    for item in random_animal_dict_keys[:10]:
        guesses = 0
        print(animal_dict[item])
        guess = input("Guess the animal!\n")
        if guess.lower() == item:
            score += 1
        else:
            guesses += 1
            while guesses < 3:
                guess = input("That's not quite right, try again please!\n")
                if guess.lower() == item:
                    score += 1
                    break
                else:
                    guesses += 1
    print(f"Final score is {score}!")

if __name__ == "__main__":
    main()