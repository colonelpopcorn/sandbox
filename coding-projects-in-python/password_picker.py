#!/usr/bin/python3
import argparse
import string
import urllib.request
import os.path
import random
from enum import Enum
from lib.common import get_and_save_to_file, get_text_from_file

adjectives_path = "./data/adjectives.txt"
adjectives_url = "https://gist.githubusercontent.com/hugsy/8910dc78d208e40de42deb29e62df913/raw/eec99c5597a73f6a9240cab26965a8609fa0f6ea/english-adjectives.txt"
nouns_path = "./data/nouns.txt"
nouns_url = "https://gist.githubusercontent.com/trag1c/f74b2ab3589bc4ce5706f934616f6195/raw/5aa7de70fc83664017cb97dd02cbf6dc76b9e4a3/nouns.txt"

class PasswordType(Enum):
    passphrase = 'passphrase'
    password = 'password'

    def __str__(self):
        return self.value

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--length", "-l", help="How many characters the password should be.", type=int)
    parser.add_argument("--mode", "-m", type=PasswordType, choices=list(PasswordType), help="The mode to use, defaults to passphrase.", default="passphrase", required=False)
    check_cache()
    arguments = parser.parse_args()
    if arguments.mode == PasswordType('passphrase'):
        generate_passphrase()
    else:
        generate_password(arguments.length)
    

def generate_passphrase():
    range_zero_to_one_hundred = range(0,100)
    strip_whitespace = lambda str: str.replace("\n", "").capitalize()
    adjectives = list(map(strip_whitespace, get_text_from_file(adjectives_path)))
    nouns = list(map(strip_whitespace, get_text_from_file(nouns_path)))
    random_adjective = random.choice(adjectives)
    random_noun = random.choice(nouns)
    random_number = random.choice(range_zero_to_one_hundred)
    print(random_adjective + random_noun + str(random_number))

def generate_password(length):
    password = []
    valid_characters = string.ascii_letters + string.digits + string.punctuation
    for x in range(0, length):
        password.append(random.choice(valid_characters))
    print("".join(password))



def check_cache():
    if not os.path.exists("./data"):
        os.makedirs("./data", exist_ok=True)
    # print("Checking cache...")
    if not os.path.exists(nouns_path):
        get_and_save_to_file(nouns_url, nouns_path)
    if not os.path.exists(adjectives_path):
        get_and_save_to_file(adjectives_url, adjectives_path)

if __name__ == "__main__":
    main()