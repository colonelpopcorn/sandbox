import urllib.request

def get_and_save_to_file(url, filename):
    try:
        with urllib.request.urlopen(url) as response:
            with open(filename, 'wb') as file:
                file.write(response.read())
                print(f"Response saved to '{filename}' successfully.")
    except urllib.error.URLError as e:
        print(f"Failed to get the response: {e}")

def get_text_from_file(file_path):
    try:
        with open(file_path, 'r') as file:
            string_list = file.readlines()
            return string_list
    except FileNotFoundError:
        print("File not found.")
    except Exception as e:
        print(f"An error occurred: {e}")