#!/usr/bin/python3
import os
import random
import urllib.request
from lib.common import get_and_save_to_file, get_text_from_file
words_path = "./data/words.txt"
words_url = "https://raw.githubusercontent.com/dwyl/english-words/master/words.txt"

def main():
    check_cache()
    heart_emoji = '❤ '
    word_list = list(map(lambda str: str.replace("\n", ""), get_text_from_file(words_path)))
    word_to_guess = random.choice(word_list)
    word_display = "_" * len(word_to_guess)
    running = True
    bad_guesses = 0
    max_bad_guesses = 9
    while running:
        lives_left = (max_bad_guesses - bad_guesses)
        if lives_left <= 0:
            print("No more lives left you lose!")
            running = False
            continue
        print("Here's your life total: " + (heart_emoji * lives_left))
        letter_guess = input("Give me a letter guess!")
        if len(letter_guess) != 1:
            print("I didn't get that, would you try again?")
            continue
        if word_to_guess.find(letter_guess) != -1:
            print(f"Correct! {letter_guess} is in the target word!")
            word_display = replace_chars_in_word_display(word_to_guess, letter_guess, word_display)
            if word_display.find("_") == -1:
                print("You won!")
                running = False
            print(word_display)
        else:
            bad_guesses += 1
            print("Oops! that letter is not in the list! Try again!")

def replace_chars_in_word_display(word_to_guess, letter_guess, word_display):
    for idx in find_idx(word_to_guess, letter_guess):
        word_display = replace_str_index(word_display, idx, letter_guess)
    return word_display

def find_idx(str, ch):
    yield [i for i, c in enumerate(str) if c == ch]

def replace_str_index(text,index=[0],replacement=''):
    temp_list = list(text)
    for idx in index:
        temp_list[idx] = replacement
    return "".join(temp_list)

def check_cache():
    if not os.path.exists("./data"):
        os.makedirs("./data", exist_ok=True)
    # print("Checking cache...")
    if not os.path.exists(words_path):
        get_and_save_to_file(words_url, words_path)


# TODO: Need to add the following:
# - Add underbars with life total
# - Cache letters already guessed
if __name__ == "__main__":
    main()