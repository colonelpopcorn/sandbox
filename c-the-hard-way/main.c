#include <stdio.h>
#include "./greeter.c"

int main(int argc, char* argv[]) {
    printf("Count of args is %d\n", argc);
    struct Greeter greeter;
    greeter.message = "Hello";
    greeter.name = argv[1];
    printf("%s, %s!\n", greeter.message, greeter.name == NULL ? "World" : greeter.name);
    return 0;
}
