const cheerio = require("cheerio");
let url = "https://www.myabandonware.com/browse/platform/windows";

const parseGames = (elements) => {
  const newList = [];
  for (let i = 0; i < elements.length; i++) {
    const gameObj = {};
    const element = elements[i];
    if (!element.attribs.class.includes("c-item-game--has-store")) {
      gameObj.gameName = element.children[1].children[0].data;
      gameObj.gameHref = element.children[1].attribs.href;
      newList.push(gameObj);
    }
  }
  return newList;
};

const finalList = [];
let shouldContinue = true;
let iterationNumber = 0;

const checkShouldContinue = (html) => {
  iterationNumber++;
  const numberOfGamesPerPage = 15;
  const headerTotal = html("div.box:nth-child(3) > h3:nth-child(2)")
    .text()
    .split(" ");
  const totalNumberOfGames = parseInt(headerTotal[headerTotal.length - 1]);
//   console.log(totalNumberOfGames);
  const gamesFetchedSoFar = iterationNumber * numberOfGamesPerPage;
  shouldContinue = gamesFetchedSoFar < totalNumberOfGames;
//   console.log(
//     `On page ${iterationNumber}, got ${gamesFetchedSoFar}, ${
//       shouldContinue ? "Continuing..." : "all done!"
//     }`
//   );
  return html;
};

async function main() {
  while (shouldContinue) {
    const pageUrl = `${url}/${
      iterationNumber === 0 ? "" : `page/${iterationNumber + 1}/`
    }`;
    // console.log(pageUrl);
    const pageList = await fetch(pageUrl)
      .then((response) => response.text())
      .then((text) => cheerio.load(text))
      .then(checkShouldContinue)
      .then((html) => html(".items.games > .item.itemListGame.c-item-game"))
      .then(parseGames)
      // .then(console.log)
      .catch((err) => console.error(err));
    finalList.push(pageList);
  }
  console.log(JSON.stringify(finalList.flat()));
}

main();
