#! /usr/bin/env node
const array = require('../data/steam_activations.json');

console.log(array.filter(game => !(game.gameName.toLowerCase().includes(' demo'))).reverse());