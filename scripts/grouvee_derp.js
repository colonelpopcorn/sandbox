const FINSIHED_SHELF_ID = '566700';
const COOKIE = process.env.GROUVEE_COOKIE || '';
const searchUrl = (query) =>
  `https://www.grouvee.com/search/auto_complete/?q=${query}`;

const json = require('./darkadia_finished_games.json');

const headers = {
  // 'User-Agent':
  //   'Mozilla/5.0(Windows NT 10.0; Win64; x64; rv: 104.0) Gecko/20100101 Firefox/104.0',
  // Accept: 'application/json, text/javascript, */*; q=0.01',
  // 'Accept-Language': 'en-US, en; q=0.5',
  // 'Accept-Encoding': 'gzip, deflate, br',
  'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
  // 'X-Requested-With': 'XMLHttpRequest',
  // 'Content-Length': 114,
  // Origin: 'https://www.grouvee.com',
  // Referer: 'https://www.grouvee.com',
  // Connection: 'keep-alive',
  Cookie: COOKIE,
  // 'Sec-Fetch-Dest': 'empty',
  // 'Sec-Fetch-Mode': 'cors',
  // 'Sec-Fetch-Site': 'same-origin',
};

// json
//   .map(({ data }) => data)
//   .forEach((game) => {
//     fetch(searchUrl(game.name), { headers })
//       .then((res) => res.json())
//       .then(console.log);
//   });

json
  .map(({ data }) => data)
  .forEach((game) => {
    fetch(searchUrl(game.name), { headers })
      .then((res) => res.json())
      .then((grouveeGames) => {
        const matchingGame = grouveeGames.results.find(
          (innerGame) =>
            innerGame.name.toLowerCase() === game.name.toLowerCase()
        );
        if (matchingGame === undefined) {
          console.warn(`Could not find ${game.name} in Grouvee!`);
        } else {
          const body = `game_id=${matchingGame.id}&shelf_id=${FINSIHED_SHELF_ID}&csrfmiddlewaretoken=N0NO8KBg2BOqWeRAVnz5Pvj3tIcTivMowypCYXtLRM475g2CDE2Pih28Nhcnr9Tq`;
          fetch('https://www.grouvee.com/shelves/toggle/', {
            method: 'POST',
            headers,
            body,
          })
            .then((res) => res.json())
            .then((response) => {
              console.log(response);
              if (response.removed) {
                // put it back!!
                fetch('https://www.grouvee.com/shelves/toggle/', {
                  method: 'POST',
                  headers,
                  body,
                })
                  .then((res) => res.json())
                  .then(console.log);
              }
            });
        }
      });
  });
