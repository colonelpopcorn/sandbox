import org.apache.commons.io.IOUtils
import java.nio.charset.StandardCharsets
import groovy.json.JsonSlurper

def flowFile = session.get()
if(!flowFile) return
def appId = flowFile.getAttribute('flowfile.content.0')
def jsonSlurper = new JsonSlurper()
def sqlScript = """
"""

def doubleUpSingleQuote(str) {
    return str.replace("'", "''");
}

session.read(flowFile, { inputStream ->
    def rawString = IOUtils.toString(inputStream, StandardCharsets.UTF_8)
    def row = jsonSlurper.parseText(rawString)
    sqlScript = """
        INSERT INTO public.gog_games (
            appid, "name", raw_response
        ) 
        VALUES 
        (
            ${appId}, '${doubleUpSingleQuote(row.title)}',
            '${doubleUpSingleQuote(rawString)}' :: json
        )
        on conflict (appid) do update set
        "name" = '${doubleUpSingleQuote(row.title)}',
        raw_response = '${doubleUpSingleQuote(rawString)}' :: json;
    """
} as InputStreamCallback)

flowFile = session.putAttribute(flowFile, 'sqlScript', sqlScript)
session.transfer(flowFile,REL_SUCCESS)
