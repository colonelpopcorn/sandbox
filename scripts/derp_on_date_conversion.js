var content = "";
var monthMap = {
  Jan: 1,
  Feb: 2,
  Mar: 3,
  Apr: 4,
  May: 5,
  Jun: 6,
  Jul: 7,
  Aug: 8,
  Sep: 9,
  Oct: 10,
  Nov: 11,
  Dec: 12,
};

function tryWithDefault(func, defaultValue) {
  try {
    return func();
  } catch (ex) {
    return defaultValue;
  }
}

function convertStrToDate(dateStr) {
  const [month, dayWithComma, year] = dateStr.split(" ");
  return `${monthMap[month]}/${dayWithComma.replace(",", "")}/${year}`;
}

function createCSV(row) {
  return (content += `"${tryWithDefault(
    () => convertStrToDate(row.children[0].innerText),
    row.children[0].innerText
  )}","${row.children[1].innerText.replace("Remove\n", "")}"\n`);
}

document
  .querySelectorAll(".account_table > tbody > tr")
  .forEach(createCSV);
console.log(content);
