#!/usr/bin/env python

import argparse

# Parse the name argument
parser = argparse.ArgumentParser()
parser.add_argument("-a", "--name", type=str, help="Name to print")
args = parser.parse_args()

# Print hello and the name
print(f"Hello {args.name}!")
