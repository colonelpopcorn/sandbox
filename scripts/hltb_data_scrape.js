function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}
// const { Client } = require("pg");
// const client = new Client({
//   user: "vidyagames",
//   host: "192.168.13.35",
//   database: "postgres",
//   password: process.env.POSTGRES_PASSWORD || "",
//   port: 5432,
// });
// let dbQueryResult;
// client.connect()
//   .then(function (err) {
//     if (err) throw err;
//   })
//   .then(() => client.query("SELECT * FROM public.steam_games;"))
//   .then((result) => dbQueryResult = result.rows)
//   .then(() => client.end())
//   .then(() => console.log("Querying done!"))
//   .finally(() => console.log(dbQueryResult));

// const STEAM_API_KEY = process.env.STEAM_API_KEY;
// const STEAM_ID = process.env.STEAM_ID;

// const steamUrl = `https://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key=${STEAM_API_KEY}&steamid=${STEAM_ID}&include_appinfo=true&format=json`;

// fetch(steamUrl)
// .then(res => res.text())
// .then(console.log);

// const hltbSearchUrl = "https://howlongtobeat.com/api/search";
const MILLIS_IN_A_MINUTE = 60 * 1000;

// const hltbSearchBody = (gameName) => {
//   return {
//     searchType: "games",
//     searchTerms: gameName.split("s"),
//     searchPage: 1,
//     size: 20,
//     searchOptions: {
//       games: {
//         userId: 0,
//         platform: "",
//         sortCategory: "popular",
//         rangeCategory: "main",
//         rangeTime: { min: null, max: null },
//         gameplay: { perspective: "", flow: "", genre: "" },
//         rangeYear: { min: "", max: "" },
//         modifier: "",
//       },
//       users: { sortCategory: "postcount" },
//       lists: { sortCategory: "follows" },
//       filter: "",
//       sort: 0,
//       randomizer: 0,
//     },
//   };
// };

// const headers = {
//   Host: "howlongtobeat.com",
//   "User-Agent":
//     "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/114.0",
//   Accept: "*/*",
//   "Accept-Language": "en-US,en;q=0.5",
//   "Accept-Encoding": "gzip, deflate, br",
//   Referer: "https://howlongtobeat.com/",
//   "Content-Type": "application/json",
//   "Content-Length": 408,
//   Origin: "https://howlongtobeat.com",
//   "Sec-Fetch-Dest": "empty",
//   "Sec-Fetch-Mode": "cors",
//   "Sec-Fetch-Site": "same-origin",
//   TE: "trailers",
// };

// const listOfGamesToFetch = require("../data/steam_games.json").response.games;
// const main = async () => {
//   for (const game of listOfGamesToFetch) {
//     const body = JSON.stringify(hltbSearchBody(game.name));
//     await sleep(3000);
//     fetch(hltbSearchUrl, {
//       body,
//       method: "POST",
//       headers: { ...headers, "Content-Length": Buffer.byteLength(body) },
//     })
//       .then((res) => res.json())
//       .then((res) =>
//         console.log(JSON.stringify({ ...game, hltbResults: res.data }))
//       )
//       .catch((result) => console.error(result));
//   }
// };

// const games = require("../data/hltb_data_scrape.json");

// const calculatePercentageCompleted = (game) => {
//   const completionMain = game.hltbResults.find(
//     (innerGame) => innerGame.game_name.toLowerCase() === game.name.toLowerCase()
//   )?.comp_main;
//   const timeToBeatMainStory = !!completionMain
//     ? completionMain * 1000
//     : undefined;
//   delete game.hltbResults;
//   const finalNumber =
//     timeToBeatMainStory !== undefined
//       ? (game.playtime_forever * MILLIS_IN_A_MINUTE) / timeToBeatMainStory
//       : "unknown";
//   return finalNumber !== "unknown"
//     ? { ...game, percentComplete: Math.round(finalNumber * 100) }
//     : { ...game, percentComplete: 0 };
// };

// const finalArr = [];
// for (const game of games) {
//   finalArr.push(calculatePercentageCompleted(game));
// }
// console.log(
//   JSON.stringify(
//     finalArr.sort((b, a) => a.percentComplete - b.percentComplete),
//     null,
//     2
//   )
// );

const newJson = require('../data/hltb_data_scrape_next_step.json');

newJson.forEach(game => {
  console.log(`insert into steam_game_attributes (appid, attribute_name, attribute_value) values (${game.appid}, 'percentComplete', ${game.percentComplete});`);
})