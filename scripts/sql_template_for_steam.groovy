import org.apache.commons.io.IOUtils
import java.nio.charset.StandardCharsets
import groovy.json.JsonSlurper

def flowFile = session.get()
if(!flowFile) return
def jsonSlurper = new JsonSlurper()
def sqlScript = """
"""

def doubleUpSingleQuote(str) {
    return str.replace("'", "''");
}

session.read(flowFile, { inputStream ->
    def rawString = IOUtils.toString(inputStream, StandardCharsets.UTF_8)
    def row = jsonSlurper.parseText(rawString)
    sqlScript = """
        INSERT INTO public.steam_games (
            appid, "name", playtime_forever, img_icon_url, 
            playtime_windows_forever, playtime_mac_forever, 
            playtime_linux_forever, raw_response
        ) 
        VALUES 
        (
            ${row.appid}, '${doubleUpSingleQuote(row.name)}', ${row.playtime_forever}, 
            '${row.img_icon_url}', ${row.playtime_windows_forever}, 
            ${row.playtime_mac_forever}, ${row.playtime_linux_forever}, 
            '${doubleUpSingleQuote(rawString)}' :: json
        )
        on conflict (appid) do update set
        "name" = '${doubleUpSingleQuote(row.name)}', 
        playtime_forever = ${row.playtime_forever}, 
        img_icon_url = '${row.img_icon_url}', 
        playtime_windows_forever = ${row.playtime_windows_forever}, 
        playtime_mac_forever = ${row.playtime_mac_forever}, 
        playtime_linux_forever = ${row.playtime_linux_forever}, 
        raw_response = '${doubleUpSingleQuote(rawString)}' :: json;
    """
} as InputStreamCallback)

flowFile = session.putAttribute(flowFile, 'sqlScript', sqlScript)
session.transfer(flowFile,REL_SUCCESS)
