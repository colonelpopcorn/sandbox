const http = require("https");
async function makeWeatherRequest(request) {
  return new Promise((resolve, reject) => {
    var str = "";
    const { lat, lon, units } = request;
    var options = {
      host: "api.openweathermap.org",
      path: `/data/2.5/weather?appid=e5325cef4606581d0a556361d3ddb2b9&lat=${lat}&lon=${lon}&units=${units}`,
    };

    callback = function (response) {
      //another chunk of data has been received, so append it to `str`
      response.on("data", function (chunk) {
        str += chunk;
      });

      //the whole response has been received, so we resolve it here
      response.on("end", function () {
        resolve(str);
      });
      response.on("error", (e) => {
        reject(e.message);
      });
    };

    http.request(options, callback).end();
  });
}

makeWeatherRequest({
  lat: 34.0258744,
  lon: -84.5374217,
  units: "metric",
}).then(console.log);
