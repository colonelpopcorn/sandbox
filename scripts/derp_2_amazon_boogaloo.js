const zeroTo = (n) => [...Array(n).keys()];
function listFibonacciUnderFourMillion() {
  let fibonacci = [0, 1];
  let newFib = 0;
  let i = 2;
  while (newFib <= 4_000_000) {
    newFib = fibonacci[i - 2] + fibonacci[i - 1];
    fibonacci[i] = newFib;
    i++;
  }
  return fibonacci;
}
const sumNumbers = (total, current) => {
  return total + current;
};

const factors = (number) =>
  Array.from(Array(number + 1), (_, i) => i).filter((i) => number % i === 0);

function isPrime(n) {
  if (isNaN(n) || !isFinite(n) || n % 1 || n < 2) return false;
  var m = Math.sqrt(n); //returns the square root of the passed value
  for (var i = 2; i <= m; i++) if (n % i == 0) return false;
  return true;
}

function largestPrimeFactor(n) {
  var i = 2;
  while (i <= n) {
    if (n % i == 0) {
      n /= i;
    } else {
      i++;
    }
  }
  return i;
}

const answer = largestPrimeFactor(600_851_475_143);
console.log(answer);
