/**
 * JSON.stringify(Array.prototype.slice.call(document.getElementsByClassName('wikitable'))
    .flatMap(innerTemp => Array.prototype.slice.call(innerTemp.getElementsByTagName('tbody')[0].children))
.map(row => Array.prototype.slice.call(row.getElementsByTagName('td')))
.map(rowArr => ({ fileExtension: rowArr[0]?.innerText, name: rowArr[1]?.innerText, system: rowArr[2]?.innerText, notes: rowArr[3]?.innerText})))
 */

// const json = require('./file-extensions.json');
// json.forEach((x) => {
//   const yamlStr = `- '${x.fileExtension}' # ${x.name} for ${x.system} -- ${x.notes}`;
//   console.log(yamlStr);
// });

// const gameyFinconfig = require('./gameyfin-config.json');
// const results = {};
// gameyFinconfig.gameyfin['file-extensions'].forEach((extension) => {
//   if (results[extension]) {
//     results[extension] += 1;
//   } else {
//     results[extension] = 1;
//   }
// });
// console.log(Object.entries(results).filter(([key, value]) => value !== 1));

// const igdbJson = require('./igdb-platforms.json');
// igdbJson
//   .sort((a, b) => a.id - b.id)
//   .forEach((json) =>
//     console.log(
//       `- ${json.id} # ${json.name} ${
//         json.alternative_name == undefined ? '' : `or ${json.alternative_name}`
//       }`
//     )
//   );

const initialPlatforms = [
  6, 23, 33, 24, 22, 3, 14, 137, 37, 4, 20, 159, 18, 21, 130, 7, 8, 9, 48, 38,
  46, 165, 35, 64, 29, 32, 58, 19, 47, 5, 41, 11, 12,
];
console.log(initialPlatforms.sort((a, b) => a - b));
