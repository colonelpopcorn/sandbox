const DARKADIA_COOKIE = process.env.DARKADIA_COOKIE || '';
if (DARKADIA_COOKIE === '') {
  console.log('I need cookies!');
  process.exit(11);
}
const darkadiaUrl = (pageNumber) =>
  `https://darkadia.com/ajax/account/get_games?uid=6586&loved=0&owned=0&played=0&playing=0&finished=1&mastered=0&dominated=0&shelved=0&pileofshame=0&genre=&platform=&tag=&search=&sort=1&view=shelf&page=${pageNumber}`;
const gbIDForGame = 4913;
const headers = {
  // ":authority": "darkadia.com",
  // ":method": "GET",
  // ":path": "/ajax/account/ get_games ? uid = 6586 & loved=0 & owned=0 & played=0 & playing=0 & finished=1 & mastered=0 & dominated=0 & shelved=0 & pileofshame=0 & genre=& platform=& tag=& search=& sort=1 & view=shelf & page=0",
  // ":scheme": "https",
  accept: 'application/json, text/javascript, */*; q=0.01',
  'accept-encoding': 'gzip, deflate, br',
  'accept-language': 'en-US, en; q = 0.9',
  cookie: DARKADIA_COOKIE,
  'if-modified-since': 'Tue, 30 Aug 2022 17: 34: 36 GMT',
  'if-none-match': 'W/"1661880876',
  referer: 'https://darkadia.com/member/colonelpopcorn/library',
  'sec-ch-ua':
    '"Chromium";v="104", " Not A;Brand";v="99", "Google Chrome";v="104"',
  'sec-ch-ua-mobile': '?0',
  'sec-ch-ua-platform': 'macOS',
  'sec-fetch-dest': 'empty',
  'sec-fetch-mode': 'cors',
  'sec-fetch-site': 'same-origin',
  'user-agent':
    'Mozilla/5.0(Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36(KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36,',
  'x-requested-with': 'XMLHttpRequest',
};
const run = async () => {
  let hasMore = true;
  let pageNumber = 0;
  const finalResults = [];
  while (hasMore) {
    const { ok, games, total, more, nextPage } = await fetch(
      darkadiaUrl(pageNumber),
      { headers }
    ).then((response) => response.json());
    if (ok) {
      hasMore = more;
      pageNumber = nextPage;
      finalResults.push(games);
    } else {
      console.error('Failed to be ok, iunno!');
    }
  }
  const flatMappedResults = finalResults.flat();

  console.log(JSON.stringify(flatMappedResults, 2, null));
};

run();